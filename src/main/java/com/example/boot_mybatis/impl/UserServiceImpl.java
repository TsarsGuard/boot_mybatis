package com.example.boot_mybatis.impl;

import com.example.boot_mybatis.dao.UserDao;
import com.example.boot_mybatis.entity.User;
import com.example.boot_mybatis.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService {

    @Resource(name = "userDao")
    private UserDao userDao;

    public User queryUserList(Integer id) {
        System.out.println("1");
        User i = userDao.queryById(id);
        return i;
    }

    public void insertTest(User user) {

        userDao.insert(user);
    }

    @Override
    public Integer queryCount() {

        return userDao.queryCount();
    }

    @Override
    public Integer batchInsert(List<User> user) {
        long startTime = System.currentTimeMillis();

        Integer list = userDao.batchInsert2(user);
        long endTime = System.currentTimeMillis();    //获取结束时间
        System.out.println("插入的时间" + (endTime - startTime));
        for (User u : user) {
            u.setAge("100");
        }
        startTime = System.currentTimeMillis();
        userDao.batchUpdateCaseNum(user);
        endTime = System.currentTimeMillis();    //获取结束时间
        System.out.println("更新的时间" + (endTime - startTime));
        return list;
    }
    
}
