package com.example.boot_mybatis.controller;

import com.example.boot_mybatis.entity.User;
import com.example.boot_mybatis.service.UserService;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TwoController implements Runnable {
    @Resource(name = "userServiceImpl")
    private UserService userService;

    public String batchInsert() {
        List<User> list = new ArrayList<>();
        for (Integer i = 0; i < 55555; i++) {
            User u2 = new User();
            u2.setName("victor");
            u2.setAge(i.toString());
            list.add(u2);
        }
        userService.batchInsert(list);
        System.out.println();
        return "success";
    }

    private void say() {
        System.out.println("two");
    }

    @Override
    public void run() {
        say();
    }
}