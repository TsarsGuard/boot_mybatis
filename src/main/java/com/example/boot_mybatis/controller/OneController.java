package com.example.boot_mybatis.controller;

import com.example.boot_mybatis.entity.User;
import com.example.boot_mybatis.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OneController implements Runnable {
    @Resource(name = "userServiceImpl")
    private UserService userService;

    public String insertUser() {
        User user = new User();
        user.setName("zhaozhihao");
        user.setAge("22");
        User u2 = new User();
        u2.setName("victor");
        u2.setAge(userService.queryCount().toString());
        userService.insertTest(u2);
        return "success4";
    }

    private void say() {
        System.out.println("one");
    }

    @Override
    public void run() {
        say();
    }
}