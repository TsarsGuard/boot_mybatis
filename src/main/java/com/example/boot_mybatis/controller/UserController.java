package com.example.boot_mybatis.controller;

import com.example.boot_mybatis.entity.User;
import com.example.boot_mybatis.service.UserService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("/query")
public class UserController {
    @Resource(name = "userServiceImpl")
    private UserService userService;

    @RequestMapping("/queryUser")
    public String query() {
        User user = userService.queryUserList(1);
        System.out.println();
        return user.getId() + user.getName();
    }

    @RequestMapping("/insertUser")
    public synchronized String insertUser() {
        User user = new User();
        user.setName("zhaozhihao");
        user.setAge("22");
        User u2 = new User();
        u2.setName("victor");
        u2.setAge(userService.queryCount().toString());
        userService.insertTest(u2);
        return "success4";
    }

    @RequestMapping("/batchInsert")
    public String batchInsert() {
        List<User> list = new ArrayList<>();
        for (Integer i = 0; i < 50000; i++) {
            User u2 = new User();
            u2.setName("victor");
            u2.setAge("121");
            list.add(u2);
        }
        Integer code = userService.batchInsert(list);
        System.out.println(list);
        return "success";
    }

    @RequestMapping("/batchInsert3")
    public String batchInsert3() {
        User u2 = new User();
        u2.setName("victor");
        u2.setAge("121");
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 55555; i++) {
            list.add(u2);
        }
        Integer code = userService.batchInsert(list);
        System.out.println();
        return "success3";
    }

    @RequestMapping("/batchInsert2")
    public String batchInsert2() {
        User u2 = new User();
        u2.setName("victor");
        u2.setAge("121");
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(u2);
        }

        for (int i = 0; i < 2; i++) {
            ExecutorService fixedThreadPool = Executors.newFixedThreadPool(2);
            fixedThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        batchInsert();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println();
                }
            });
        }
        return "success";
    }

    @RequestMapping("/insertUser2")
    public String insertUser2() {
        for (int i = 0; i < 10; i++) {
            ExecutorService fixedThreadPool = Executors.newFixedThreadPool(4);
            fixedThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    insertUser();
                    System.out.println(userService.queryCount());
                }
            });
        }
        System.out.println();
        return "success";
    }

    @RequestMapping("/thread")
    public void thread() {
        OneController one = new OneController();
        TwoController two = new TwoController();
        Thread thread = new Thread(one);
        Thread thread2 = new Thread(two);
        thread.start();
    }

}
