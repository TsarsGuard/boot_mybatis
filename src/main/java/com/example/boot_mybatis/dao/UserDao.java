package com.example.boot_mybatis.dao;

import com.example.boot_mybatis.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDao")
public interface UserDao {
    User queryById(Integer id);

    void insert(User user);

    Integer queryCount();

    Integer batchInsert(@Param("myUser") List<User> user);

    /**
     * 批量插入返回
     *
     * @param user
     * @return
     */
    Integer batchInsert2(List<User> user);

    void batchUpdateCaseNum(List<User> user);
}
