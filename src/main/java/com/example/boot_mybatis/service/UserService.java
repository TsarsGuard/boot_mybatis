package com.example.boot_mybatis.service;

import com.example.boot_mybatis.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    User queryUserList(Integer id);

    void insertTest(User user);

    Integer queryCount();

    Integer batchInsert(List<User> user);
}
