package com.example.boot_mybatis.entity;

import lombok.Data;

@Data
public class User {
    String name;
    String age;
    Integer id;
}
