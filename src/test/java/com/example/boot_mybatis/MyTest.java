package com.example.boot_mybatis;

import com.example.boot_mybatis.entity.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MyTest {
    @Test
    public void test() {
        List<User> list = new ArrayList<>();
        User user = new User();
        user.setId(12);
        user.setName("zhaozhihao");
        user.setAge("22");
        User u2 = new User();
        u2.setId(13);
        u2.setName("victor");
        u2.setAge("121");
        list.add(user);
        list.add(u2);
        for (User u : list) {
            u.setName("change");
        }
        System.out.println();
    }

    @Test
    public void ThreadTest() {
        One oneThread = new One();
        Two two = new Two();
        for (int i = 0; i < 20; i++) {
            Thread thread = new Thread(oneThread);
            Thread thread2 = new Thread(two);
            thread.start();
            thread2.start();
        }
    }

}
